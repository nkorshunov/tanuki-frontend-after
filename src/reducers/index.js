import { combineReducers } from 'redux';
import page from './page';

export default (storages, isServer) =>
  combineReducers({
    page: page(storages)
  });
