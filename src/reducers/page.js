import { SET_LANGUAGE } from '~/constants/page';
import { persistReducer } from 'redux-persist';

const initialState = {
  language: 'ru'
};

function page(state = initialState, action) {
  switch (action.type) {
    case SET_LANGUAGE:
      return {
        ...state,
        language: action.language
      };
    default:
      return state;
  }
}

export default storages => {
  const pagePersistConfig = {
    key: 'page',
    storage: storages.cookieStorage,
    whitelist: 'language'
  };

  return persistReducer(pagePersistConfig, page);
};
