import { CookieStorage } from 'redux-persist-cookie-storage';
import Cookies from 'cookies-js';

export default function clientCookieStorage() {
  const setCookieOptions = {
    httpOnly: false
  };

  return new CookieStorage(Cookies, { setCookieOptions });
}
