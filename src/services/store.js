import { persistStore } from 'redux-persist';

let globalStore = null;

export const persistStorePromise = store => {
  return new Promise((resolve, reject) => {
    const persistor = persistStore(store, {}, () => {
      registerStore(store);
      resolve(persistor);
    });
  });
};

export const registerStore = store => {
  globalStore = store;
};

export const getStore = () => globalStore;
