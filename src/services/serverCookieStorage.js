import {
  CookieStorage,
  NodeCookiesWrapper
} from 'redux-persist-cookie-storage';

import Cookies from 'cookies';

export default function serverCookieStorage(req, res) {
  const cookieJar = new NodeCookiesWrapper(new Cookies(req, res));
  const setCookieOptions = {
    httpOnly: false
  };
  return new CookieStorage(cookieJar, {
    setCookieOptions
  });
}
