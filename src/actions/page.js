import { SET_LANGUAGE } from '~/constants/page';

export function setLanguage(language) {
  return {
    type: SET_LANGUAGE,
    language
  };
}
