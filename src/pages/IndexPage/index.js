import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as pageActions from '~/actions/page';

class IndexPage extends Component {
  static async getInitialProps({ store, req }) {
    const { language } = req.query;

    if (language) {
      store.dispatch(pageActions.setLanguage(language));
    }

    return {};
  }

  render() {
    const { page } = this.props;
    return <div>Current language: {page.language}</div>;
  }
}

const mapStateToProps = ({ page }) => ({
  page
});

export default connect(mapStateToProps)(IndexPage);
