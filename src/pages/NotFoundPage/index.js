import React, { Component } from 'react';

export default class NotFoundPage extends Component {
  static async getInitialProps({ res }) {
    res.status(404);
    return {};
  }
  render() {
    return <div>Page not found.</div>;
  }
}
