import React from 'react';
import { renderToString } from 'react-dom/server';
import express from 'express';
import { Provider } from 'react-redux';
import { render } from '@jaredpalmer/after';
import Cookies from 'cookies';

import routes from '~/routes';
import Document from '~/Document';
import configureStore from '~/store';
import { persistStorePromise } from '~/services/store';

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST);

const server = express();
server
  .use(Cookies.express())
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR))
  .get('/*', async (req, res) => {
    try {
      const store = configureStore(undefined, { req, res });
      const persistor = await persistStorePromise(store);

      const customRenderer = node => {
        const App = <Provider store={store} children={node} />;
        const serverState = store.getState(); // using as __PRELOADED_STATE__ (See: Document.js)

        return {
          html: renderToString(App),
          serverState
        };
      };

      const html = await render({
        req,
        res,
        routes,
        assets,
        customRenderer,
        document: Document,
        store
      });

      await persistor.flush(); // write state in cookies

      res.send(html);
    } catch (error) {
      res.json(error);
    }
  });

export default server;
