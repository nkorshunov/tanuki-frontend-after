import { createStore, compose, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';

import rootReducer from './reducers';
import clientCookieStorage from './services/clientCookieStorage';
import serverCookieStorage from './services/serverCookieStorage';
import { persistStore } from 'redux-persist';
import { registerStore } from './services/store';

const isClient = typeof window !== 'undefined';

const composeEnhancers =
  (isClient && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const enhancers = composeEnhancers(
  applyMiddleware(thunkMiddleware, promiseMiddleware)
);

const makeStore = (storages, preloadedState) => {
  const store = createStore(
    rootReducer(storages, !isClient),
    preloadedState,
    enhancers
  );

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('./reducers').default;
      store.replaceReducer(nextRootReducer(storages, !isClient));
    });
  }
  return store;
};

export default function configureStore(preloadedState, { req, res } = {}) {
  const storages = {
    localStorage: require('redux-persist/lib/storage'),
    cookieStorage: isClient
      ? clientCookieStorage()
      : serverCookieStorage(req, res)
  };

  const store = makeStore(storages, preloadedState);

  if (isClient) {
    store.__persistor = persistStore(store);
    registerStore(store);
  }

  return store;
}
